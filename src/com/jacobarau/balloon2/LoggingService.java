package com.jacobarau.balloon2;

import java.util.ArrayList;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;

public class LoggingService extends Service {
	MessageAdapter ma;
	private final IBinder mBinder = new LocalBinder();
	PowerManager.WakeLock wl = null;
	
	public class LocalBinder extends Binder {
        LoggingService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LoggingService.this;
        }
    }
	
	@Override
	public IBinder onBind(Intent arg0) {
		Log.d("LoggingService", "OnBind called");
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d("LoggingService", "OnCreate called");
		
		// Keep the system from going to sleep during screen power off. We need the
		// CPU awake while we are logging location.
		// TODO: release the wakelock and possibly kill the service while not logging location.
		// TODO: refactor to use the alarm manager to wake up once in awhile and log.
		PowerManager pm = (PowerManager)getSystemService(
				Context.POWER_SERVICE);
		wl = pm.newWakeLock(
				PowerManager.PARTIAL_WAKE_LOCK,
				"Balloon Logging Service Live");
		wl.acquire();
		
		// Make the service a foreground service. This keeps Android from killing it on a whim.
		Notification notification = new Notification(R.drawable.ic_launcher, getText(R.string.service_running),
		        System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, getText(R.string.service_running),
		        getText(R.string.service_running), pendingIntent);
		startForeground(8675309, notification);
		
		// We store all events as they happen in this list, and the adapter lives here.
		// We pass the adapter to the activity when it is running to allow it to list the events.
		ArrayList<BalloonMessage> list = new ArrayList<BalloonMessage>();
		ma = new MessageAdapter(this, R.layout.running_list_item, list);
		
		// Set up the location receiver.
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.
		      ma.add(new LocationMessage(location.getLatitude(), location.getLongitude()));
		      Log.d("LoggingService","Location changed.");
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}
		  };

		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);

	}
	
	public void onDestroy() {
		super.onDestroy();
		Log.d("LoggingService", "OnDestroy called");
		wl.release();
	}
	
	public MessageAdapter getMessageAdapter() {
		return ma;
	}

}
