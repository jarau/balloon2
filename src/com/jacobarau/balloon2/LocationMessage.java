package com.jacobarau.balloon2;

public class LocationMessage extends BalloonMessage {
	double lat;
	double lon;
	
	public LocationMessage(double latitude, double longitude)
	{
		super("Lat: " + latitude + ", Lon: " + longitude);
		lat = latitude;
		lon = longitude;
	}
}
