package com.jacobarau.balloon2;

import java.util.Date;

public class BalloonMessage {
	String myText;
	Date myTime;
	
	public BalloonMessage(String text, Date time)
	{
		myText = text;
		myTime = time;
	}
	
	public BalloonMessage(String text)
	{
		this(text, new Date());
	}
	
	public String toString()
	{
		return myText;
	}
}
