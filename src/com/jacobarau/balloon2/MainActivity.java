package com.jacobarau.balloon2;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.jacobarau.balloon2.LoggingService.LocalBinder;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	LoggingService mService;
    boolean mBound = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	protected void onStart() {
		super.onStart();
		Intent intent = new Intent(this, LoggingService.class);
		startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        
        Timer t = new Timer();
        t.schedule(new TimerTask() {
        	public void run() {
        		connectAdapter();
        	}
        }, (long)1);
	}
	
	protected void connectAdapter() {
		// Hack to deal with the asynchronous nature of the service bind.
		// Should eventually use some sort of event passing from the onServiceConnected
		// event below, but didn't know how to do it easily from within ServiceConnection class.
		if (!mBound) {
			// If we haven't bound with the service yet, schedule this function to be called
			// in another second. Hopefully eventually it binds.
			Timer t = new Timer();
			t.schedule(new TimerTask() {
		        	public void run() {
		        		runOnUiThread(new Runnable() {
		        			public void run() {
		        				connectAdapter();
		        			}
		        		});
		        	}
		        }, (long)1);	
		}
		else {
			// We successfully bound.
			// Attach the message adapter to the list view.
		    final ListView listview = (ListView) findViewById(R.id.running_log);
			listview.setAdapter(mService.getMessageAdapter());
		}
	}
	
	protected void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	/** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
	
}
