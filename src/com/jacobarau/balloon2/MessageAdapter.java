package com.jacobarau.balloon2;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MessageAdapter extends ArrayAdapter<BalloonMessage> {
    HashMap<BalloonMessage, Integer> mIdMap = new HashMap<BalloonMessage, Integer>();
    LayoutInflater li;

    public MessageAdapter(Context context, int textViewResourceId,
        List<BalloonMessage> objects) {
      super(context, textViewResourceId, objects);
      for (int i = 0; i < objects.size(); ++i) {
        mIdMap.put(objects.get(i), i);
      }
      li = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
      BalloonMessage item = getItem(position);
      return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
      return true;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	// We need to create a new view, or use the "convertView" specified
    	// if non-null to display the data.
    	// For now, we will ignore the convertView parameter and just
    	// create new Views each time.
    	
    	BalloonMessage item = getItem(position);
    	View v = li.inflate(R.layout.running_list_item, null);
    	TextView tv = (TextView) v.findViewById(R.id.eventDescription);
    	tv.setText(item.myText);
    	TextView dv = (TextView) v.findViewById(R.id.eventTime);
    	dv.setText(item.myTime.toString());
    	
    	return v;
    }
    
    @Override
    public void add(BalloonMessage object)
    {
    	super.add(object);
    	mIdMap.put(object, mIdMap.size());
    }
}
